#LEX
names = { }
reserved = {
	'if' : 'IF',
	'else' : 'ELSE',
	'in'	:	'IN',
	'not'	:	'NOT',
	'and'	:	'AND',
	'or'	:	'OR',
	'if'	:	'IF',
	'while'	:	'WHILE',
	'print'	:	'PRINT',
}

tokens = [
	'ID','INT','REAL','STRING','LPAREN','RPAREN',
	'LBRACKET','RBRACKET','COMMA','TIMES','DIVIDE','MOD','POWER',
	'FDIV','ADD','MINUS','LESS','LEQUAL','EQUAL','NEQUAL','GREATER','GEQUAL','EQUALS','LCB','RCB','SEMICO'
	]+list(reserved.values())

def t_REAL(t):
	r'\d+\.\d+'
	try:
		t.value = float(t.value)
	except ValueError:
		print("Real value too large %d", t.value)
	return t

def t_INT(t):
	r'\d+'
	try:
		t.value = int(t.value)
	except ValueError:
		print("Integer value too large %d", t.value)
	return t

def t_STRING(t):
	r'\"([^\\"]|(\\.))*\"'
	skip = 0
	str = t.value[1:-1]
	new_str = ""
	for i in range(0, len(str)):
		char = str[i]
		if skip:
		    if char == "n":
		        char = "\n"
		    elif char == "t":
		        char = "\t"
		    new_str += c
		    skip = 0
		else:
		    if char == "\\":
		        skip = 1
		    else:
		        new_str += char
	t.value = new_str
	return t

def t_ID(t):
	r'[a-zA-Z_][a-zA-Z_0-9]*'
	t.type = reserved.get(t.value,'ID')
	return t

t_LPAREN		=	r'\('
t_RPAREN		=	r'\)'
t_LBRACKET		=	r'\['
t_RBRACKET		=	r'\]'
t_LCB			= 	r'\{'
t_RCB			=	r'\}'
t_TIMES			=	r'\*'
t_DIVIDE		=	r'/'
t_MOD			=	r'%'
t_POWER			=	r'\*\*'
t_FDIV			=	r'//'
t_ADD			=	r'\+'
t_MINUS			=	r'-'
t_LESS			=	r'<'
t_LEQUAL		=	r'<='
t_EQUAL 		=	r'=='
t_NEQUAL		=	r'<>'
t_GREATER		=	r'>'
t_GEQUAL		=	r'>='
t_EQUALS		=	r'='
t_COMMA			=	r'\,'
t_SEMICO		=	r'\;'

t_ignore = " \t"

def t_newline(t):
	r'\n'
	t.lexer.lineno += t.value.count("\n")

def t_error(t):
	print("Illegal character '%s'" % t.value[0])
	t.lexer.skip(1)

import ply.lex as lex
lexer = lex.lex()

#AST
import operator
from types import LambdaType

class InstructionList:
	def __init__(self,children =None):
		if children is None:
			children = []
		self.children = children

	def __len__(self):
		return len(self.children)
	def __iter__(self):
		return iter(self.children)
	def __repr__(self):
		return '<InstructionList {0}>'.format(self.children)
	def eval(self):
		ret=[]
		for n in self:
			if isinstance(n,ExitStatement):
				return n

			res = n.eval()

			if isinstance(res,ExitStatement):
				return res
			elif res is not None:
				ret.append(res)
		return ret
class BaseExpression:
	def eval(self):
		raise NotImplementedError()
class ExitStatement(BaseExpression):
	def __iter__(self):
		return []
	def eval(self):
		pass

class ReturnStatement(ExitStatement):
    def __init__(self, expr: BaseExpression):
        self.expr = expr

    def __repr__(self):
        return '<Return expr={0}>'.format(self.expr)

    def eval(self):
        return full_eval(self.expr)


def full_eval(expr: BaseExpression):
    """
    Fully evaluates the passex expression returning it's value
    """

    while isinstance(expr, BaseExpression):
        expr = expr.eval()

    return expr


class Primitive(BaseExpression):
    def __init__(self, value):
        self.value = value

    def __repr__(self):
        return '<Primitive "{0}"({1})>'.format(self.value, self.value.__class__)

    def eval(self):
        return self.value
class List(BaseExpression):
	def __init__(self,values: InstructionList):
		self.values = values

	def eval(self):
		return self.values.eval()

class Index(BaseExpression):
	def __init__(self, array, index):
		self.array = array
		self.index = index
	def eval(self):
		return self.array.eval()[self.index.eval()]
class Index_multi(BaseExpression):
	def __init__(self, array, index1, index2):
		self.array = array
		self.index1 = index1
		self.index2 = index2
	def eval(self):
		return self.array.eval()[self.index1.eval()][self.index2.eval()]

class ArrayAssign(BaseExpression):
	def __init__(self, array, index, value):
		self.array = array
		self.index = index
		self.value = value
	def eval(self):
		self.array.eval()[self.index.eval()] = self.value.eval()

class Identifier(BaseExpression):
	def __init__(self,name):
		self.name = name
	def __repr__(self):
		return '<Identifier {0}>'.format(self.name)
	def assign(self, val):
		names[self.name] = val
	def eval(self):
		return names[self.name]
class Assignment(BaseExpression):
	def __init__(self, identifier:Identifier, val):
		self.identifier =identifier
		self.val =val
	def __repr__(self):
		return '<Assignment sym={0}; val={1}>'.format(self.identifier, self.val)
	def eval(self):
		self.identifier.assign(self.val.eval())
class BinaryOperation(BaseExpression):
	__operations = {
		'+':operator.add,
		'-':operator.sub,
		'*': operator.mul,
		'**': operator.pow,
		'/': operator.truediv,
		'%': operator.mod,
		'//': operator.floordiv,

		'>': operator.gt,
		'>=': operator.ge,
		'<': operator.lt,
		'<=': operator.le,
		'==': operator.eq,
		'<>': operator.ne,

		'and': lambda a,b: a.eval() and b.eval(),
		'or' : lambda a,b: a.eval() or b.eval(),

	}
	def __repr__(self):
		return '<BinaryOperation left ={0} right={1} operation="{2}">'.format(self.left, self.right, self.op)
	def __init__(self,left,right,op):
		self.left = left
		self.right = right
		self.op = op
	def eval(self):
		left = None
		right = None

		try:
			op = self.__operations[self.op]
			if isinstance(op, LambdaType):
				return op(self.left,self.right)
			left = self.left.eval()
			right = self.right.eval()
			return op(left,right)
		except TypeError:
		    fmt = (left.__class__.__name__, left, self.op, right.__class__.__name__, right)
		    raise InterpreterRuntimeError("Unable to apply operation (%s: %s) %s (%s: %s)" % fmt)

class UnaryOperation(BaseExpression):
	__operations = {
		'not': operator.not_
	}
	def __repr__(self):
	    return '<Unary operation: operation={0} expr={1}>'.format(self.operation, self.expr)
	def __init__(self, operation, expr:BaseExpression):
		self.operation = operation
		self.expr = expr

	def eval(self):
		return self.__operations[self.operation](self.expr.eval())
class InExpression(BaseExpression):
	def __init__(self, a, b):
		self.a = a
		self.b = b
	def eval(self):
		return self. a.eval() in self.b.eval()
class PrintStatement(BaseExpression):
	def __init__(self,items:InstructionList):
		self.items = items
	def __repr__(self):
		return '<Print {0}>'.format(self.items)
	def eval(self):
		print(*self.items.eval(), end='',sep='')

class While(BaseExpression):
	def __init__(self,condition,body):
		self.condition = condition
		self.body = body
	def eval(self):
		while self.condition.eval():
			if isinstance(self.body.eval(),ExitStatement):
				break
class If(BaseExpression):
	def __init__(self, condition: BaseExpression, truestmt:InstructionList,elsestmt=None):
		self.condition = condition
		self.truestmt = truestmt
		self.elsestmt = elsestmt
	def eval(self):
		if self.condition.eval():
			return self.truestmt.eval()
		elif self.elsestmt is not None:
			return self.elsestmt.eval()

#YACC
import ply.yacc as yacc

precedence = (
	('left','AND','OR','NOT'),
	('left','IN','LESS','LEQUAL','EQUAL','NEQUAL','GREATER','GEQUAL'),
	('left','ADD','MINUS'),
	('left','TIMES','DIVIDE','MOD','FDIV','POWER'),
	)



def p_statement_list(p):
	'''statement_list : statement
					  | statement_list statement'''
	if len(p)==2:
		p[0] = InstructionList([p[1]])
	else:
		p[1].children.append(p[2])
		p[0]=p[1]
def p_block(p):
	'''statement_list : LCB statement_list RCB'''
	p[0] = p[2]
def p_statement(p):
	'''statement : identifier
				 | expression
				 | if_statement
				 '''
	p[0] = p[1]

def p_identifier(p):
	'''identifier : ID'''
	p[0] = Identifier(p[1])
#exit 
def p_primitive(p):
	'''primitive : INT
				| REAL
				| STRING'''
	if isinstance(p[1], BaseExpression):
		p[0] = p[1]
	else:
		p[0] = Primitive(p[1])
def p_list(p):
	'''expression_l : LBRACKET arguments RBRACKET'''
	p[0] = List(p[2])
def p_binary_op(p):
	'''expression : expression ADD expression
				  | expression MINUS expression
				  | expression TIMES expression
				  | expression DIVIDE expression
				  | expression POWER expression
				  | expression FDIV expression
				  | expression MOD expression'''
	p[0] = BinaryOperation(p[1],p[3],p[2])

def p_boolean_operator(p):
	'''expression : expression EQUAL expression
				  | expression NEQUAL expression
				  | expression GREATER expression
				  | expression GEQUAL expression
				  | expression LESS expression
				  | expression LEQUAL expression
				  | expression AND expression
				  | expression OR expression'''
	p[0] = BinaryOperation(p[1],p[3],p[2])

def p_unary_operation(p):
	'''expression : NOT expression'''
	p[0] = UnaryOperation(p[1],p[2])

def p_paren(p):
	'''expression : LPAREN expression RPAREN'''
	p[0] = p[2] if isinstance(p[2], BaseExpression) else Primitive(p[2])
def p_assignable(p):
    '''
    assignable : primitive
               | expression
    '''
    p[0] = p[1]
def p_assign(p):
	'''expression : identifier EQUALS assignable SEMICO'''
	p[0] = Assignment(p[1],p[3])

def p_in_expression(p):
	'''expression : expression IN expression'''
	p[0] = InExpression(p[1],p[3])

def p_arguments(p):
	'''arguments : arguments COMMA expression
				 | expression
				 |'''
	if len(p) ==2:
		p[0] = InstructionList([p[1]])
	elif len(p)==1:
		p[0] = InstructionList()
	else:
		p[1].children.append(p[3])
		p[0] = p[1]
def p_index(p):
	'''expression : identifier LBRACKET expression RBRACKET
				  | identifier LBRACKET expression RBRACKET LBRACKET expression RBRACKET'''
	if len(p)==5:
		p[0] = Index(p[1],p[3])
	else:
		p[0] = Index_multi(p[1],p[3],p[6])

def p_array_assign(p):
	'''statement : identifier LBRACKET expression RBRACKET EQUALS expression SEMICO'''
	p[0] = ArrayAssign(p[1],p[3],p[6])

def p_print_statement(p):
	'''statement : PRINT arguments SEMICO'''
	p[0] = PrintStatement(p[2])

def p_while_loop(p):
	'''statement : WHILE expression LCB statement_list RCB'''
	p[0] = While(p[2],p[4])

def p_ifstatement(p):
	'''if_statement : IF expression statement_list'''
	p[0] = If(p[2],p[3])
def p_ifelse_statement(p):
	'''if_statement : IF expression statement_list ELSE statement_list'''
	p[0] = If(p[2],p[3],p[5])
def p_expression(p):
    '''
    expression : primitive
               | STRING
               | identifier
               | expression_l
    '''
    p[0] = p[1]


def p_error(p):
    if p is not None:
        raise ParserSyntaxError("Syntax error at line %d, illegal token '%s' found" % (p.lineno, p.value))

    raise ParserSyntaxError("Unexpected end of input")


parser = yacc.yacc(errorlog=yacc.NullLogger())
class InterpreterException(Exception):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return self.message


class SymbolNotFound(InterpreterException):
    pass


class UnexpectedCharacter(InterpreterException):
    pass


class ParserSyntaxError(InterpreterException):
    pass


class DuplicateSymbol(InterpreterException):
    pass


class InterpreterRuntimeError(InterpreterException):
    pass


class InvalidParamCount(InterpreterRuntimeError):
    pass
import sys
import pprint

f = open(sys.argv[1]).read()
res = parser.parse(f)
for node in res.children:
	node.eval()
